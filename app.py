"""Lima's main WSGI application.

...

Attributes:
    app: An instance of :class:`flask.Flask`, this is Lima's WSGI application.
"""

from flask import Flask, request, jsonify, render_template, abort
# import werkzeug.urls
# from fava.core import FavaLedger
from ariadne import load_schema_from_path, make_executable_schema, \
    graphql_sync, snake_case_fallback_resolvers, QueryType
from ariadne.constants import PLAYGROUND_HTML
from datetime import date

from api.queries import allEntries_resolver, allAccounts_resolver, \
    getAccount_resolver, getAccounts_resolver, getFavoriteAccounts_resolver, \
    getAccountsByType_resolver, getTransaction_resolver, getTransactions_resolver, \
    getPositions_resolver
# from api.queries import schema

app = Flask(__name__, static_url_path='')

query = QueryType()
query.set_field("allEntries", allEntries_resolver)
query.set_field("allAccounts", allAccounts_resolver)
query.set_field("getAccount", getAccount_resolver)
query.set_field("getFavoriteAccounts", getFavoriteAccounts_resolver)
query.set_field("getAccountBalance", getAccount_resolver)
query.set_field("getAccountBalances", getAccounts_resolver)
query.set_field("getAccountsByType", getAccountsByType_resolver)
query.set_field("getTransaction", getTransaction_resolver)
query.set_field("getTransactions", getTransactions_resolver)
query.set_field("getPositions", getPositions_resolver)

type_defs = load_schema_from_path("schema.graphql")
schema = make_executable_schema(
    type_defs, query, snake_case_fallback_resolvers
)

@app.context_processor
def template_context():
    """Inject variables into the template context."""
    return dict(favorites=getFavoriteAccounts_resolver({}, {})) # ledger=FavaLedger

@app.route('/')
def index():
    """ Journal """
    # select id, account, other_accounts, date, flag, payee, narration, position, balance, change WHERE date >= today() AND date <= 2022-01-31 ORDER BY date
    return render_template('index.html', page_title=date.today().strftime('%A %b %d, %Y'), journal=getTransactions_resolver({}, {}))

@app.route("/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200

@app.route("/graphql", methods=["POST"])
def graphql_server():
    data = request.get_json()
    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )
    status_code = 200 if success else 400

    # print(result)
    return jsonify(result), status_code

@app.route("/account/<name>")
@app.route("/account/<name>/<subreport>")
def account(name: str, subreport: str = "journal") -> str:
    """The account report."""
    if subreport in ["journal", "balances", "changes"]:
        return render_template(
            "account.html", account_name=name, subreport=subreport
        )
    return abort(404)

# Taxes
# Income Statement
# Balance Sheet
# Query
# Editor
# Errors
# Import
# Options
# Help

# @app.route("/jump")
# def jump() -> werkzeug.wrappers.response.Response:
#     """Redirect back to the referer, replacing some parameters.
#     This is useful for sidebar links, e.g. a link ``/jump?time=year``
#     would set the time filter to `year` on the current page.
#     When accessing ``/jump?param1=abc`` from
#     ``/example/page?param1=123&param2=456``, this view should redirect to
#     ``/example/page?param1=abc&param2=456``.
#     """
#     url = werkzeug.urls.url_parse(request.referrer)
#     qs_dict = url.decode_query()
#     for key, values in request.args.lists():
#         if values == [""]:
#             try:
#                 del qs_dict[key]
#             except KeyError:
#                 pass
#         else:
#             qs_dict.setlist(key, values)

#     redirect_url = url.replace(
#         query=werkzeug.urls.url_encode(qs_dict, sort=True)
#     )
#     return redirect(werkzeug.urls.url_unparse(redirect_url))