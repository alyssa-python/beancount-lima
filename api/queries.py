from os import path, getcwd
from functools import reduce
from beancount.query import query
from beancount import loader

# from ariadne import load_schema_from_path, make_executable_schema, \
#     graphql_sync, snake_case_fallback_resolvers, QueryType

filename = path.join(getcwd(), 'data', 'ledger.beancount')

# query = QueryType()

SQL_ACCOUNT_FIELDS = r"""
    account,
    GETITEM(OPEN_META(account), "favorite") as favorite,
    GETITEM(OPEN_META(account), "name") as name,
    open_date(account) as opened"""

def convert_sum_to_balance(currency = 'USD'):
    return f"convert(sum(position), '{currency}') AS balance"

# def account():
#     return {
#         "account_id": 
#         "name": 
#         "favorite": bool()
#         "balance": 
#         "open": .strftime(DATE_FORMAT)
#     }

DATE_FORMAT = "%Y-%m-%d"

def allEntries_resolver(obj, info, date="today()"):
    """ Returns all postings filtered by date. """

    bql_query = f" \
        SELECT \
          {SQL_ACCOUNT_FIELDS}, \
          {convert_sum_to_balance()} \
        WHERE date <= {date};"

    # print(bql_query)
    return resolver(
        bql_query, 
        lambda rows: [
            { 
                "account": { 
                    "account_id": account, 
                    "name": name, 
                    "favorite": bool(favorite), 
                    "balance": balance,
                    "open": opened.strftime(DATE_FORMAT),
                }, 
            } for [account, favorite, name, opened, balance] in rows], 
        key="entries"
    )

def allAccounts_resolver(obj, info, date="today()"):
    """ Returns all accounts referenced in Postings, filtered by date. """

    bql_query = f" \
        SELECT \
          {SQL_ACCOUNT_FIELDS}, \
          {convert_sum_to_balance()} \
        WHERE open_date(account) <= {date};"

    # print(bql_query)
    return resolver(
        bql_query, 
        lambda rows: [
            { 
                "account_id": account, 
                "name": name, 
                "favorite": bool(favorite),
                "open": opened.strftime(DATE_FORMAT),
                "balance": balance,
                # "postings": [],
            } for [account, favorite, name, opened, balance] in rows], 
        key="accounts",
    )

def getAccount_resolver(obj, info, account, date="today()"):
    """ Returns account by 'id' and filtered by date. """

    bql_query = f" \
        SELECT \
          {SQL_ACCOUNT_FIELDS}, \
          {convert_sum_to_balance()} \
        WHERE account = \"{account}\" \
          AND date <= {date};"

    # print(bql_query)
    return resolver(
        bql_query, 
        # TODO: fix this: coerce Decimal (list) to Float
        lambda rows: { 
            "account_id": account, 
            "favorite": bool(rows[0][1]), 
            "name": rows[0][2], 
            "open": rows[0][3].strftime(DATE_FORMAT),
            "balance": rows[0][4],
        }, # TODO: handle list index out of range with 0.00 balance
        key="account"
    )

def getAccounts_resolver(obj, info, names, date="today()"):
    """ Returns accounts by 'id' and filtered by date. """

    where = " OR ".join([f"account='{name}'" for name in names])
    bql_query = f" \
        SELECT \
          {SQL_ACCOUNT_FIELDS}, \
          {convert_sum_to_balance()} \
        WHERE {where} \
          AND date <= {date};"

    # print(bql_query)
    return resolver(
        bql_query, 
        # TODO: fix this: coerce Decimal (list) to Float
        lambda rows: [
            { 
                "account_id": account,
                "name": name, 
                "balance": balance, 
                "favorite": bool(favorite),
                "open": opened.strftime(DATE_FORMAT),
            } for [account, favorite, name, opened, balance] in rows], # TODO: handle list index out of range with 0.00 balance
        key="accounts"
    )

def getFavoriteAccounts_resolver(obj, info, date="today()"):
    """ Returns accounts filtered by 'favorite' and date. """

    bql_query = f" \
        SELECT \
          {SQL_ACCOUNT_FIELDS}, \
          {convert_sum_to_balance()} \
        WHERE \
          GETITEM(OPEN_META(account), \"favorite\") = \"true\" \
          AND date < {date};"

    # print(bql_query)
    return resolver(
        bql_query, 
        # TODO: fix this: coerce Decimal (list) to Float
        lambda rows: [
            { 
                "account_id": account,
                "name": name, 
                "balance": balance, 
                "favorite": bool(favorite),
                "open": opened.strftime(DATE_FORMAT),
            } for [account, favorite, name, opened, balance] in rows], # TODO: handle list index out of range with 0.00 balance
        key="accounts"
    )

def getAccountsByType_resolver(obj, info, types, date="today()"):
    """ Returns accounts filtered by AccountType and date. """

    where = " OR ".join([f"account ~ '{name}'" for name in types])
    bql_query = f" \
        SELECT \
          {SQL_ACCOUNT_FIELDS}, \
          {convert_sum_to_balance()} \
        WHERE {where} \
          AND date <= {date};"

    # print(bql_query)
    return resolver(
        bql_query, 
        # TODO: fix this: coerce Decimal (list) to Float
        lambda rows: [
            { 
                "account_id": account,
                "name": name, 
                "balance": balance, 
                "favorite": bool(favorite),
                "open": opened.strftime(DATE_FORMAT),
            } for [account, favorite, name, opened, balance] in rows], # TODO: handle list index out of range with 0.00 balance
        key="accounts"
    )

def getTransaction_resolver(obj, info, id):
    """ Returns all postings for a transaction by ID 
    
    getTransaction(id: "") {
        transaction {
            id,
            date,
            flag,
            payee,
            narration,
            meta {
                ...
            }
            postings {
                flag
                account {
                    id
                    name
                    favorite
                    open
                }
                amount
            }
        }
    }
    """

    bql_query = f" \
        SELECT \
          id, date, flag, payee, narration, posting_flag, position, \
          {SQL_ACCOUNT_FIELDS} \
        WHERE id = \"{id}\";"

    # print(bql_query)
    return resolver(
        bql_query,
        lambda rows: {
            "id": id,
            "date": rows[0][1].strftime(DATE_FORMAT),
            "flag": rows[0][2],
            "payee": rows[0][3],
            "narration": rows[0][4],
            "postings": [{
                "flag": posting_flag,
                "account": {
                    "account_id": account,
                    "name": name, 
                    "favorite": bool(favorite),
                    "open": opened.strftime(DATE_FORMAT),
                },
                "amount": position
            } for [transaction_id, date, flag, payee, narration, posting_flag, position, account, favorite, name, opened] in rows]
        },
        key="transaction"
    )

def getPostings_resolver(obj, info, before="today()", after="DATE_ADD(today(),-7)",):
    """ Returns filtered postings """

    bql_query = f" \
        SELECT \
          id, date, flag, payee, narration, \
          {convert_sum_to_balance()}, \
          {SQL_ACCOUNT_FIELDS} \
        WHERE date >= {after} AND date <= {before}"

    # print(bql_query)
    return resolver(
        bql_query,
        lambda rows: [
            {
                "id": transaction_id,
                "account": {
                    "account_id": account,
                    "name": name, 
                    "favorite": bool(favorite),
                    "open": opened.strftime(DATE_FORMAT),
                },
                "date": date.strftime(DATE_FORMAT),
                "flag": flag,
                "payee": payee,
                "narration": narration,
                "amount": position_usd,
                # "postings": [ position_usd ],
            } for [transaction_id, date, flag, payee, narration, position_usd, account, favorite, name, opened, ] in rows],
        key="postings"
    )

def getTransactions_resolver(obj, info, before="today()", after="DATE_ADD(today(),-7)"):
    """ Returns filtered transactions """

    bql_query = f" \
        SELECT \
          id, date, flag, payee, narration, \
          {convert_sum_to_balance()}, \
          {SQL_ACCOUNT_FIELDS} \
        WHERE date >= {after} AND date <= {before}"

    def reduce_transactions(prev, nxt):
        # print(prev)
        index = next((index for (index, lst) in enumerate(prev) if lst["id"] == nxt[0]), None)
        # print(index)

        # print(nxt)

        if(index is not None):
            # print(prev[index])
            prev[index]['postings'].append({
                "account": {
                    "account_id": nxt[6],
                    "name": nxt[7], 
                    "favorite": bool(nxt[8]),
                    "open": nxt[9].strftime(DATE_FORMAT),
                },
                "amount": nxt[5]
            })
        else:
            prev.append({
                "id": nxt[0],
                "date": nxt[1].strftime(DATE_FORMAT),
                "flag": nxt[2],
                "payee": nxt[3],
                "narration": nxt[4],
                "postings": [
                    {
                        "account": {
                            "account_id": nxt[6],
                            "favorite": bool(nxt[7]),
                            "name": nxt[8], 
                            "open": nxt[9].strftime(DATE_FORMAT),
                        },
                        "amount": nxt[5]
                    }
                ]
            })

        return prev

    # print(bql_query)
    return resolver(
        bql_query,
        lambda rows: reduce(reduce_transactions, rows, []),
        key="transactions"
    )

def getPositions_resolver(obj, info, before="today()", after="DATE_ADD(today(),-7)"):
    bql_query = f" \
        SELECT \
          date, sum(position), \
          {SQL_ACCOUNT_FIELDS} \
        WHERE account ~ \"Expenses\" or account ~ \"Income\""

    # print(bql_query)
    return resolver(
        bql_query,
        lambda rows: [{
            "date": date.strftime(DATE_FORMAT),
            "amount": position,
            "account": {
                "account_id": account,
                "name": name, 
                "favorite": bool(favorite),
                "open": opened.strftime(DATE_FORMAT),
            },

        } for [date, position, account, favorite, name, opened] in rows],
        key="positions"
    )

def helper(bql_query, numberify):
    entries, errors, options_map = loader.load_file(filename)
    # if len(errors) > 0: Raise Exception(errors)
    rtypes, rows = query.run_query(entries, options_map,
        bql_query, numberify=numberify
    )

    # print("===")
    # print(rtypes)
    # print(errors)
    # print(rows)
    return errors, rtypes, rows

def resolver(bql_query, transformer, key="entries", numberify=True):
    try:
        errors, rtypes, rows = helper(bql_query, numberify=numberify)

        payload = {
            "success": True,
            key: transformer(rows)
        }

    except Exception as error:
        payload = {
            "success": False,
            "errors": [str(error)]
        }

    # print("=====")
    # print(payload)
    return payload

# query.set_field("allEntries", allEntries_resolver)
# query.set_field("allAccounts", allAccounts_resolver)
# query.set_field("getAccount", getAccount_resolver)
# query.set_field("getFavoriteAccounts", getFavoriteAccounts_resolver)
# query.set_field("getAccountBalance", getAccount_resolver)
# query.set_field("getAccountBalances", getAccounts_resolver)

# type_defs = load_schema_from_path("schema.graphql")
# schema = make_executable_schema(
#     type_defs, query, snake_case_fallback_resolvers
# )