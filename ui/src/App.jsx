import * as React from 'react'
import { render } from 'react-dom'
import { useRoutes } from 'hookrouter'
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";

import { Layout } from './modules/Layout'
import { Home } from './modules/Home'
import { Account } from './modules/Account'

import "./styles/style.css"
import "./styles/app.css"

// these need to match Flask routes as defined in app.py
const routes = {
    '/': () => <Layout renderFilters={false}><Home /></Layout>,
    '/account/:account': ({ account }) => <Layout><Account accountId={account} /></Layout>,
    '/account/:account/:subReport': ({ account, subReport }) => <Layout><Account accountId={account} subReport={subReport} /></Layout>,
};

const Router = () => useRoutes(routes)

const client = new ApolloClient({
    uri: '/graphql',
    cache: new InMemoryCache()
});

const App = () => 
    <ApolloProvider client={client}>
        <Router />
    </ApolloProvider>

render(<App />, document.getElementById('root'));