import * as React from 'react'

export const Balances = ({ nivoData }) => 
    <>
        <h2>Account Balances</h2>

        <div className="pure-g">
            <div className="pure-u-1 pure-u-sm-1-2">
                <h3>Income / Liabilities</h3>
                <ul>
                    {nivoData?.accounts.map(({ accountId, name, balance }) => <li key={accountId}><a href={`/account/${accountId}`}>{name || accountId}</a>: ${balance}</li> )}
                </ul>
            </div>

            <div className="pure-u-1 pure-u-sm-1-2">
                <h3>Expenses / Assets</h3>
                <ul>
                    
                </ul>
            </div>
        </div>
    </>