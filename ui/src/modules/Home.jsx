import * as React from 'react'
import { ResponsiveCalendar } from '@nivo/calendar'

import { useQuery, gql } from "@apollo/client"

import { Balances } from './Balances'
import { SpendingByCategory } from './SpendingByCategory'

const from = '2022-01-02'
const to = '2023-01-01'

const CHANGES_BY_DATE = gql`
    query changesByDate {
        changes: getPositions(before: "2022-01-01", after: "2019-12-31") {
            success
            positions {
                date
                amount
            }
        }

        favorites: getFavoriteAccounts {
            success
            accounts {
                accountId
                name
                balance
            }
        }

        expenses: getAccountsByType(types: [Expenses, Liabilities]){
            success
            accounts {
                accountId
                balance
            }
        }
    }
`

const Tooltip = ({
    day, date, value, color, x, y, size
}) =>
    <ul style={{ height: 100 }}>
        <li>day: {day}</li>
        <li>date: {date}</li>
        <li>value: {value}</li>
        <li>color: {color}</li>
        <li>x: {x}</li>
        <li>y: {y}</li>
        <li>size: {size}</li>
    </ul>

export const Home = () => {
    const { loading, error, data } = useQuery(CHANGES_BY_DATE);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    // render Net Worth in page H1

    return <>
        <div className="pure-hidden-xs" style={{ height: 200 }}>
            <ResponsiveCalendar
                data={data?.changes?.positions.map(({ date, amount }) => ({ day: date, value: amount }))}
                from={from}
                to={to}
                emptyColor="#eeeeee"
                colors={[ '#61cdbb', '#97e3d5', '#e8c1a0', '#f47560' ]}
                margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
                yearSpacing={40}
                monthBorderColor="#ffffff"
                dayBorderWidth={2}
                dayBorderColor="#ffffff"
                tooltip={(values) => Tooltip(values)}
                onClick={console.warn}
                legends={[
                    {
                        anchor: 'bottom-right',
                        direction: 'row',
                        translateY: 36,
                        itemCount: 4,
                        itemWidth: 42,
                        itemHeight: 36,
                        itemsSpacing: 14,
                        itemDirection: 'right-to-left'
                    }
                ]}
            />
        </div>


        <div className="content">
            <Balances nivoData={data?.favorites} />
            <SpendingByCategory nivoData={data?.expenses} />
        </div>
    </>
}