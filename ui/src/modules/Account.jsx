import * as React from 'react'

export const Account = ({ accountId, subReport = 'journal' }) => {
    return `Account ${subReport}: ${accountId}`
}