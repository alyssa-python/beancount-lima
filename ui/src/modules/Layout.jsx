import * as React from 'react'

export const Layout = ({ children, renderFilters=true }) => 
    <>
    {renderFilters &&
        <form>
            <input placeholder="time" />
            <input placeholder="account" />
            <input placeholder="filter" />
        </form>}
        
        <article>
            {children}
        </article>
    </>