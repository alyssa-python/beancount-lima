import * as React from 'react'
import { ResponsiveSunburst } from '@nivo/sunburst'

import { set as _set, get as _get, map as _map, merge as _merge } from 'lodash'

const recurse = (iterable) => {
    if(iterable.children) {
        recurse(iterable.children)
    } else {
        console.warn(iterable) // should be an array

        if(iterable.length > 1) {
            iterable = iterable.reduce((prev, next, i, a) => {
                if(prev.length > 0 ) {
                    const _idx = prev.findIndex(({ id }) => id === next.id)

                    if(_idx > -1) {
                        prev[_idx] = _merge(prev[_idx], next)
                        return prev
                    }
                }
                prev.push(next)
                return prev
            }, [])
        }
    }

    return iterable
}


/**
 * @see https://nivo.rocks/sunburst/
 * @param {string} id 
 * @param {*} accounts - [ { accountId: "Root:SubAccount:...", balance } ]
 * @returns { id, children: [{ id, children: [{ id, value }] }] }
 */
 const mapToSunburst = (id, accounts) => {
    // loop through all categories
    const reduced = accounts.reduce((prev, next) => {
        // `root` is top-level, one of Assets, Liabilities, Income, Expenses or Equity
        // `leafs` is subAccounts, infinitely nesting
        const [root, ...leafs] = next.accountId.split(':');

        // get top-level account, or create it
        const rootAccount = _get(prev, root, { id: root, children: [] })

        // console.warn(next.accountId, leafs, next.balance)

        // reduce leaf accounts into nested children
        const reduced = leafs.reduceRight((prv, nxt) => {
            // console.log(prv, nxt)
            
            // parent
            if(prv.id) {
                return { id: nxt, children: [prv] }
            }

            // child
            prv.id = nxt
            return prv

        }, { value: Math.abs(next.balance), })

        rootAccount.children.push(reduced)

        prev[root] = rootAccount

        return prev
    }, {})

    // TODO: RECURSIVELY de-dupe children
    const children = _map(reduced, (value, key) => ({ id: key, ...value }) )
    const deduped = children.map((curr, idx, arr) => {
        // return recurse(curr)

        if(curr.children.length > 1) {
            curr.children = curr.children.reduce((prev, next, i, a) => {
                if(prev.length > 0 ) {
                    const _idx = prev.findIndex(({ id }) => id === next.id)

                    if(_idx > -1) {
                        prev[_idx] = _merge(prev[_idx], next)
                        return prev
                    }
                }
                prev.push(next)
                return prev
            }, [])
        }

        return curr
    })

    // console.error(deduped)

    return {
        id,
        children: deduped
    }
}

export const SpendingByCategory = ({ nivoData }) =>
    <>
        <h2>Spending by Category</h2>
        <a href="/account/...">Current Statement</a>

        <div className="pure-g">
            <div className="pure-u-1 pure-u-sm-3-5">
                <div style={{ height: 500 }}>
                    <ResponsiveSunburst
                        data={mapToSunburst('spendingByCategory', nivoData?.accounts)}
                        // margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
                        cornerRadius={2}
                        borderColor={{ theme: 'background' }}
                        // colors={{ scheme: 'nivo' }}
                        childColor={{ from: 'color', modifiers: [ [ 'brighter', 0.3 ] ] }}
                        enableArcLabels={true}
                        arcLabel={function(e){return e.id+" \n("+e.value+")"}}
                        arcLabelsSkipAngle={10}
                        arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.4 ] ] }}
                    />
                </div>
            </div>
            <div className="pure-u-1 pure-u-sm-2-5">
                <ul>
                {nivoData?.accounts.map(({ accountId, balance }) => 
                    <li key={accountId}>
                        <a href={`/account/${accountId}`}>{accountId}</a>: ${balance}
                    </li>)}
                </ul>
            </div>
        </div>

    </>
